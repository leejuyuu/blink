import math
from pathlib import Path
from typing import Callable, NamedTuple, Optional, Tuple, Union

import numpy as np
import scipy.interpolate
import scipy.ndimage
import skimage.transform

import blink.image_processing as imp


def _smooth_truncate(image, w):
    if not (w % 2):
        # _smooth_truncate should only accept odd number
        # because IDL SMOOTH function silently add one to even numbers
        raise ValueError(
            f"Kernel width must be odd number for _smooth_truncate. Input: {w}"
        )
    kernel = np.ones(w) / w
    smoothed = scipy.ndimage.convolve1d(image, kernel, 0, mode="nearest")
    smoothed = scipy.ndimage.convolve1d(smoothed, kernel, 1, mode="nearest")
    return smoothed


def _make_block_background(image, block_size):
    """
    The image is split into blocks, and the background is then calculated
    as the minimum of each block, then rebinned to the size of the original
    image then smoothed.
    """
    if image.shape[0] % block_size or image.shape[1] % block_size:
        raise ValueError(
            f"Image shape {image.shape} is not integral multiples of block_size "
            f"({block_size})"
        )
    small_image = _find_block_min(image, block_size)
    bkg = _rebin(small_image, block_size)
    # TODO: the smooth kernel size should depend on the block size
    bkg = _smooth_truncate(bkg, 21)
    return bkg


def _find_block_min(image, block_size):
    """Find the minimum values in each block"""
    y_size = image.shape[0] // block_size
    x_size = image.shape[1] // block_size
    values = np.zeros((y_size, x_size))
    for i in range(y_size):
        for j in range(x_size):
            values[i][j] = image[
                i * block_size : (i + 1) * block_size,
                j * block_size : (j + 1) * block_size,
            ].min()
    return values


def _rebin(small_image, block_size):
    """
    Implements the IDL REBIN functions for the special case that expands the input
    image.
    """
    shape = (small_image.shape[0] * block_size, small_image.shape[1] * block_size)
    y = np.arange(0, shape[0], block_size)
    x = np.arange(0, shape[1], block_size)
    inter = scipy.interpolate.RegularGridInterpolator((y, x), small_image)
    bkg = np.zeros(shape)
    idx = (
        slice(0, shape[0] - block_size + 1),
        slice(0, shape[1] - block_size + 1),
    )
    yg, xg = np.mgrid[idx]
    bkg[idx] = inter((yg, xg))

    # The last block will not be interpolated, use edge value
    for i in y:
        bkg[i : i + block_size, x[-1] : x[-1] + block_size] = small_image[
            i // block_size
        ][-1]

    for j in x:
        bkg[y[-1] : y[-1] + block_size, j : j + block_size] = small_image[-1][
            j // block_size
        ]
    return bkg


def _pick_spot_from_combined_image(
    combined_im, combined_im_threshold, inner_r, outer_r, circle_factor
):
    height, width = combined_im.shape
    median = np.median(combined_im)
    thresholded_combined_im = np.where(
        combined_im < median + combined_im_threshold, 0, combined_im
    )
    circular_idx = _generate_circular_mask(inner_r, outer_r)
    mask_r = int(np.ceil(outer_r))
    peaks = []
    # Ignore edges where we cannot fit a circular mask
    for x in range(mask_r, width - mask_r):
        for y in range(mask_r, height - mask_r):
            if thresholded_combined_im[y, x] <= 0:
                continue
            aoi_im = thresholded_combined_im[y - 3 : y + 4, x - 3 : x + 4]
            # TODO: The argmax is related to order of the elements where there are
            # repeats, should probably use another method
            yc, xc = np.unravel_index(np.argmax(aoi_im), aoi_im.shape)
            if xc != 3 or yc != 3:  # Peak not at AOI center
                continue
            idx = (
                y + circular_idx[0],
                x + circular_idx[1],
            )
            if np.any(
                combined_im[idx] > math.floor(median + circle_factor * aoi_im[yc, xc])
            ):
                continue
            peaks.append([x, y])
    return np.array(peaks, dtype=int)


def calculate_aoi_background(
    image: np.ndarray,
    aois: Union["imp.Aois", np.ndarray],
    block_size: int,
    kernel_size: int = 3,
):
    """
    Calculate the image background from block minima.

    Split the image into blocks, find the minima in each block, and use
    the minima array to interpolate into the original image size as the
    background image.

    The AOI background is the value of the pixel nearest to the center of the AOI.

    This background calculation method originated from Taekjip Ha lab and was modified
    by Dr. Kai-Chun Chang from Prof. Jin-Der Wen lab and various lab members from
    Hung-Wen Li lab. Please see
    https://github.com/Ha-SingleMoleculeLab/Raw-Data-Analysis and
    https://gitlab.com/hwligroup/FRET_analysis_routines .

    Args:
        image: (M, M) array, the input image.
        aois: (N, 2) array or an Aois object, the coordinates to calculate background
              from.
        block_size: The size of block to split the image into, should be a factor of M.
        kernel_size: The box-car kernel size to smooth the interpolated background
                     image.

    Returns:
        backgrouds: (N,) array, the background value corresponding to each AOI.
    """
    # aois can be both an Aois object or a N x 2 array
    smoothed_im = _smooth_truncate(image, kernel_size)
    block_background = _make_block_background(smoothed_im, block_size)
    backgrounds = np.array([block_background[round(y), round(x)] for x, y in aois])
    return backgrounds


def load_traces(path: Path) -> np.ndarray:
    """Read a IDL script generated .traces binary file into ndarray.

    Please see
    https://github.com/Ha-SingleMoleculeLab/Raw-Data-Analysis .

    Args:
        path: The path to the .traces file.

    Returns:
        A (n_frames, n_traces) traces array contained in the file.
    """
    with open(path, "r") as f:
        n_frames = np.fromfile(f, dtype=np.int32, count=1).item()
        n_traces = np.fromfile(f, dtype=np.int16, count=1).item()
        traces = np.fromfile(f, dtype=np.int16).reshape(n_frames, n_traces)
    return traces


def _despeckle(image: np.ndarray):
    """Suppress pixels with only one non-zero neighbor in the 8-neighbors.

    Will modify the image in place, return the modified image."""

    # TODO: This function is execution order dependent, should improve in the future
    # Invert i, j for IDL order
    for j in range(1, image.shape[1] - 1):
        for i in range(1, image.shape[0] - 1):
            if image[i, j] == 0:
                continue
            count = np.count_nonzero(image[i - 1 : i + 2, j - 1 : j + 2]) - 1
            if count < 2:
                image[i, j] = 0
    return image


class ImageSplit(NamedTuple):
    """
    The namedtuple holding the channels information

    channel_a_idx: Index to slice the image array to get the channel_a subimage.
    channel_b_idx: Index to slice the image array to get the channel_b subimage.
    transform_b_to_a: A callable to tranform coordinates in channel_b to channel_a.
    """

    channel_a_idx: Tuple[slice, slice]
    channel_b_idx: Tuple[slice, slice]
    transform_b_to_a: Callable[[np.ndarray], np.ndarray]


def find_peaks(
    image: np.ndarray,
    split: "ImageSplit",
    frame: int = 0,
    frame_avg: int = 1,
    width: int = 7,
    channel: Optional[str] = None,
    bkg_std_factor: Union[int, float] = 2,
    combined_im_threshold: Union[int, float] = 25,
    inner_r: float = 3.6,
    outer_r: float = 4.2,
    circle_factor: float = 0.45,
) -> "imp.Aois":
    """
    Find peaks from local maxima using methods modified from Taekjip Ha lab.

    The background below (median + bkg_std_factor*std) is suppressed for the two
    channels individually. Speckles (bright pixels with only one bright neighbor)
    are also suppressed. Then the two channels are combined. The background noise
    from the image warping that is below (median + combined_im_threshold) is
    suppressed from the combined image.
    The peaks are found where pixels are local maxima centered in a (width, width)
    box, while also satisfy that the pixel values on a circle surrounding the peak
    are not greater than (median + AOI_max_intensity*circle_factor).

    This spot picking method originated from Taekjip Ha lab and was modified by
    Dr. Kai-Chun Chang from Prof. Jin-Der Wen lab and various lab members from
    Hung-Wen Li lab. Please see
    https://github.com/Ha-SingleMoleculeLab/Raw-Data-Analysis and
    https://gitlab.com/hwligroup/FRET_analysis_routines .

    Args:
        image: 2-D ndarray, the image array to pick spot from.
        image_split: A namedtuple containing the information of the two channels
                     and the tranform callable to convert coordinates between them.
        frame: The frame number of the image, passed to Aois.
        frame_avg: The frame average of the image, passed to Aois.
        width: The width of the resulting AOIs, passed to Aois.
        channel: The channel that the image corresponds to , passed to Aois.
        bkg_std_factor: Background below (median + bkg_std_factor*std) will be
                        suppressed to 0 separately for both channel.
        combined_im_threshold: After image warp and combination, any background noise
                               below (median + combined_im_threshold) will be suppresed
                               to 0.
        inner_r: The inner radius (inclusive) to generate a circular mask.
        outer_r: The outer radius (exclusive) to generate a circular mask.
        circle_factor: The factor to discard the peaks that are too wide. The
                       pixels on the circular mask having values greater than
                       (median + AOI_max_intensity*circle_factor) will be discarded.

    Returns:
        (N, 2) ndarray storing the (x, y) coordinates of the peaks found.
    """
    channel_a, channel_b, transform_b_to_a = split
    ch_a_median = np.median(image[channel_a])
    ch_b_median = np.median(image[channel_b])
    ch_a_std = image[channel_a].std()
    ch_b_std = image[channel_b].std()
    image[channel_a][image[channel_a] < (ch_a_median + bkg_std_factor * ch_a_std)] = 0
    image[channel_b][image[channel_b] < (ch_b_median + bkg_std_factor * ch_b_std)] = 0

    despeckled_im = _despeckle(image)

    ch_a_im = despeckled_im[channel_a]
    ch_b_im = despeckled_im[channel_b]
    # Note: the clip keyword might be able to do something, but I did not check what
    # it does
    transformed = skimage.transform.warp(
        ch_a_im, transform_b_to_a, order=3, mode="edge"
    )
    combined_im = ch_b_im + transformed

    peaks = _pick_spot_from_combined_image(
        combined_im,
        combined_im_threshold,
        inner_r,
        outer_r,
        circle_factor,
    )
    aois = imp.Aois(
        peaks, frame=frame, frame_avg=frame_avg, width=width, channel=channel
    )
    return aois.gaussian_refine(combined_im)


def _generate_circular_mask(
    inner_r: float, outer_r: float
) -> Tuple[np.ndarray, np.ndarray]:
    width = int(np.ceil(outer_r))
    grid = np.ogrid[-width : width + 1, -width : width + 1]
    dist = np.sqrt(grid[0] ** 2 + grid[1] ** 2)
    in_range = np.logical_and(dist >= inner_r, dist < outer_r)
    idx = np.nonzero(in_range)
    return tuple(arr - width for arr in idx)
