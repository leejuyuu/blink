from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

import blink.drift_correction
import blink.image_processing as imp
from blink import mapping


def quickMinMax(data):
    """
    Estimate the min/max values of *data* by subsampling.
    Returns [(min, max), ...] with one item per channel

    Copied from the pyqtgraph ImageView class
    """
    while data.size > 1e6:
        ax = np.argmax(data.shape)
        sl = [slice(None)] * data.ndim
        sl[ax] = slice(None, None, 2)
        data = data[tuple(sl)]

    if data.size == 0:
        return [(0, 0)]
    return (float(np.nanmin(data)), float(np.nanmax(data)))


def main():
    frame = 200
    datapath = Path("~/analysis_results/20211020/20211020imscroll/").expanduser()
    image_path_green = Path(
        "~/Expt_data/20211020/L2_BCDX2_647_125uM/tzu-yu00021"
    ).expanduser()
    image_path = Path(
        "~/Expt_data/20211020/L2_BCDX2_647_125uM/tzu-yu00022"
    ).expanduser()
    filestr = "L2"
    drifter = blink.drift_correction.DriftCorrector.from_npy(
        datapath / (filestr + "_driftlist.npy")
    )

    edgecolor = sns.color_palette("muted")[0]
    image_sequence = imp.ImageSequence(image_path_green)
    aois = imp.Aois.from_npz(datapath / (filestr + "_200_aoi.npz"))
    aois.channel = "green"
    print(image_sequence.time[frame])
    fig, ax = plt.subplots(figsize=(2, 2))
    image = image_sequence.get_averaged_image(frame, 10)
    scale = quickMinMax(image)
    ax.imshow(
        image,
        cmap="gray_r",
        vmin=scale[0],
        vmax=scale[1] - 200,
        interpolation="nearest",
        origin="upper",
    )
    origin = np.array([350, 100]) - 0.5  # Offset by 0.5 to the edge of pixel
    size = 73
    ax.set_axis_off()
    coords = aois.coords
    in_range = np.logical_and.reduce(
        (
            coords[:, 0] > origin[0],
            coords[:, 0] < origin[0] + size,
            coords[:, 1] > origin[1],
            coords[:, 1] < origin[1] + size,
        )
    )
    ax.scatter(
        aois.get_all_x()[in_range],
        aois.get_all_y()[in_range],
        marker="s",
        color="none",
        edgecolors=edgecolor,
        linewidth=1,
        s=70,
    )
    ax.text(
        0.05,
        0.95,
        "DNA",
        color=sns.color_palette("dark")[2],
        fontfamily="roboto",
        fontsize=12,
        fontweight="medium",
        horizontalalignment="left",
        verticalalignment="top",
        transform=ax.transAxes,
    )
    ax.set_xlim((origin[0], origin[0] + size))
    ax.set_ylim((origin[1], origin[1] + size))
    fig.savefig(
        Path(datapath / "image_temp_green.svg").expanduser(),
        dpi=1200,
        format="svg",
        bbox_inches="tight",
        pad_inches=0,
    )
    plt.close(fig)
    mapping_file_path_1 = Path(
        "~/git_repos/Imscroll-and-Utilities/data/mapping/20210208_bg_5.dat"
    ).expanduser()
    mapping_file_path_2 = Path(
        "~/git_repos/Imscroll-and-Utilities/data/mapping/20210208_rb_5.dat"
    ).expanduser()
    image_sequence = imp.ImageSequence(image_path)
    # aois = drifter.shift_aois_by_time(aois, image_sequence.time[frame * 10])
    print(image_sequence.time[frame * 10])
    mapper1 = mapping.Mapper.from_imscroll(mapping_file_path_1)
    mapper2 = mapping.Mapper.from_imscroll(mapping_file_path_2)
    mapped_aois = mapper1.map(aois, to_channel="blue")
    mapped_aois = mapper2.map(mapped_aois, to_channel="red")

    aois = mapped_aois
    fig, ax = plt.subplots(figsize=(2, 2))
    image = image_sequence.get_averaged_image(frame * 10, 10)
    print(frame * 10)
    scale = quickMinMax(image)
    ax.imshow(
        image,
        cmap="gray_r",
        vmin=scale[0],
        vmax=scale[1] - 500,
        interpolation="nearest",
        origin="upper",
    )
    ax.set_axis_off()
    ax.scatter(
        aois.get_all_x()[in_range],
        aois.get_all_y()[in_range],
        marker="s",
        color="none",
        edgecolors=edgecolor,
        linewidth=0.8,
        s=70,
        linestyle=":",
    )
    ax.text(
        0.05,
        0.95,
        "BCDX2",
        color=sns.color_palette("dark")[3],
        fontfamily="roboto",
        fontsize=12,
        fontweight="medium",
        horizontalalignment="left",
        verticalalignment="top",
        transform=ax.transAxes,
    )
    ref_aoi = imp.pick_spots(image_sequence.get_averaged_image(frame * 10, 10), 20)
    colocalized_aois = aois.remove_aois_far_from_ref(ref_aoi, 1.5)
    origin -= mapper1.map_matrix[("blue", "green")][:, 2]
    origin -= mapper2.map_matrix[("red", "blue")][:, 2]
    coords = colocalized_aois.coords
    in_range = np.logical_and.reduce(
        (
            coords[:, 0] > origin[0],
            coords[:, 0] < origin[0] + size,
            coords[:, 1] > origin[1],
            coords[:, 1] < origin[1] + size,
        )
    )
    ax.scatter(
        colocalized_aois.get_all_x()[in_range],
        colocalized_aois.get_all_y()[in_range],
        marker="s",
        color="none",
        edgecolors=edgecolor,
        linewidth=1,
        s=70,
    )
    ax.set_xlim((origin[0], origin[0] + size))
    ax.set_ylim((origin[1], origin[1] + size))
    fig.savefig(
        datapath / "image_temp_red.svg",
        dpi=1200,
        format="svg",
        bbox_inches="tight",
        pad_inches=0,
    )
    plt.close(fig)


if __name__ == "__main__":
    main()
