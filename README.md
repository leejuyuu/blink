# blink: Single-molecule fluorescence data analysis tools

Blink is a collection of single-molecule fluorescence data analysis routines,
containing mainly image processing functions for fluorescence time-series
extraction and some downstream analysis scripts.

This project was partly translated from the [Imscroll](https://github.com/gelles-brandeis/CoSMoS_Analysis),
a MATLAB project.
Blink is written in Python in the hope that it should be easier to access 
and free to use. Also, the various Python libraries and tools make developing
much easier, and it would be easier to search for help in the community.

Checkout the [Documentation](https://leejuyuu.gitlab.io/blink) to get started.
